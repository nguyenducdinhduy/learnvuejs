// import axios from 'axios' // APIを使用しない場合削除
const webpack = require("webpack");
// baseUrlは都度調整してください
const baseUrl = 'http://localhost:3000';

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s｜｛タイトルが入ります｝',
    title: 'nuxt_template',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'descriptionが入ります' },
      { hid: 'og:description', property: 'og:description', content: 'descriptionが入ります' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', size: '144x144', href: '/application.png' }
    ],
  },
  plugins: [
    '~/plugins/vue-match-heights',
    '~/plugins/vue-scrollto',
    {
      src: '~/plugins/vue-lazyload',
      ssr: false
    },
    {
      src: "~/plugins/google-map", ssr: true
    }
  ],
  modules: [
    [
      'nuxt-imagemin', {
        optipng: { optimizationLevel: 5 },
        gifsicle: { optimizationLevel: 2 }
      }
    ],
    ['@nuxtjs/pwa'], // pwa対応用プラグイン 使用しない場合削除
    ['@nuxtjs/style-resources'], // SASSの利用とGlobal CSSの読み込みに対応 使用しない場合削除
    /* フォームバリデーション用プラグイン 使用しない場合削除
    [
      'nuxt-validate', {
        lang: 'ja',
      }
    ],
    */
    /* googleタグマネージャー用プラグイン 使用しない場合削除
    [
      '@nuxtjs/google-tag-manager',
      {
        id: 'GTM-〇〇',
        pageTracking: true
      }
    ]
    */
  ],
  workbox: { // PWA用の設定 使用しない場合削除
    dev: false,
    offline: false,
    skipWaiting: true,
    clientsClaim: true,
    runtimeCaching: [
      {
        urlPattern: baseUrl,
        handler: 'cacheFirst',
        method: 'GET',
        strategyOptions: {
          cacheExpiration: {
            maxAgeSeconds: 86400,
          },
          cacheableResponse: {
            statuses: [200],
          },
        },
      },
    ],
  },
  manifest: { // PWA用の設定 使用しない場合削除
    name: 'タイトル',
    lang: 'ja',
    start_url: '/', // アプリのスタートURL
    short_name: 'アプリ名',
    title: 'タイトル',
    'og:title': 'タイトル',
    description: 'descriptionが入ります',
    'og:description': 'descriptionが入ります',
    theme_color: '#3B8070',
    background_color: '#3B8070'
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' }, //ルーティングリンクを使用する場合のローディング中のバー表示 カラーは都度変更
  /*
  ** Build configuration
  */
  build: {
    transpile: [/^vue2-google-maps($|\/)/],
    vendor: ['jquery'], // jquery使用しない場合削除
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery"
      })
    ],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  styleResources: {
    sass: [
      '~/assets/scss/0_base/_vars.scss', //共通でincludeしたいファイル
      '~/assets/scss/0_base/_mixin.scss', //共通でincludeしたいファイル
    ],
  },
  css: [
    { src: '~/assets/scss/style.scss', lang: 'scss' },
  ],
  /* 動的URLを使用する場合必要 使用しない場合削除
  generate: {
    interval: 5000,
    routes: function() {
      return axios.all([
        axios.get(baseUrl + 'APIエンドポイント'),
      ]).then( ([post]) => {
        let post_list = [];
        post.data.forEach((el) => {
          post_list.push(el.slug) //vueファイルを「_id.vue」にした場合はid
        });
        return post_list;
      })
    }
  }
  */
}

