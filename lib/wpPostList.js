import axios from 'axios'

class WpApiArchive {
  constructor (siteurl) {
    this.apiBase = `${siteurl}/wp-json`
  }
  posts ( postType1 = 'beginner', postType2 = 'method', postType3 = 'works', postType4 = 'information', postType5 = 'download', pages = 1, perPage = -1 ) {
    return axios.all([
      axios.get(`${this.apiBase}/wp/v2/${postType1}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType2}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType3}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType4}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType5}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
    ]).then( ([json1, json2, json3, json4, json5]) => {
      return { posts1: json1.data, posts2: json2.data, posts3: json3.data, posts4: json4.data, posts5: json5.data }
    }).catch(e => {
      return { error: e }
    })
  }
}



const wpPostList = new WpApiArchive(process.env.baseUrl + '/wordpress/wp')

export default wpPostList