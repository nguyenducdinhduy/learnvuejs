import axios from 'axios'

class WpApiSingle {
  constructor (siteurl) {
    this.apiBase = `${siteurl}/wp-json`
  }
  posts ( postType = 'posts', postId = '' ) {
    return axios.get(`${this.apiBase}/wp/v2/${postType}/${postId}`).then(json => {
      return { post: json.data }
    }).catch(e => {
      return { error: e }
    })
  };
  postsAndPostById ( postType = 'posts', postType1 = 'beginner', postType2 = 'method', postType3 = 'works', postType4 = 'download', postType5 = 'information', pages = 1, perPage = -1, postId = '') {
    return axios.all([
      axios.get(`${this.apiBase}/wp/v2/${postType}/${postId}`),
      axios.get(`${this.apiBase}/wp/v2/${postType1}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType2}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType3}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType4}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType5}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
    ]).then( ([json, jsons1, jsons2, jsons3, jsons4, jsons5]) => {
      return { post: json.data, posts1: jsons1.data, posts2: jsons2.data, posts3: jsons3.data, posts4: jsons4.data, posts5: jsons5.data }
    }).catch(e => {
      return { error: e }
    })
  };
  postsAndPostBySlug ( postType = 'posts', postType1 = 'beginner', postType2 = 'method', postType3 = 'works', postType4 = 'download', postType5 = 'information', pages = 1, perPage = -1, postSlug = '') {
    return axios.all([
      axios.get(`${this.apiBase}/wp/v2/${postType}?slug=${postSlug}`),
      axios.get(`${this.apiBase}/wp/v2/${postType1}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType2}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType3}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType4}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
      axios.get(`${this.apiBase}/wp/v2/${postType5}`, {
        params: {
          page: pages,
          per_page: perPage
        }
      }),
    ]).then( ([json, jsons1, jsons2, jsons3, jsons4, jsons5]) => {
      return { post: json.data[0], posts1: jsons1.data, posts2: jsons2.data, posts3: jsons3.data, posts4: jsons4.data, posts5: jsons5.data }
    }).catch(e => {
      return { error: e }
    })
  };
}

const wpPostSingle = new WpApiSingle(process.env.baseUrl + '/wordpress/wp')

export default wpPostSingle