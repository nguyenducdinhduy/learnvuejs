import Vue from "vue";
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCQFKwVLvTw3-ovzOWm1UUAHeIVyDkW2t4",
    libraries: "places"
  }
});
